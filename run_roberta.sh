# ebg
python train_roberta.py --corpus ebg --task obfuscation
python train_roberta.py --corpus ebg --task imitation

# rj
python train_roberta.py --corpus rj --task obfuscation
python train_roberta.py --corpus rj --task imitation
python train_roberta.py --corpus rj --task control
python train_roberta.py --corpus rj --task backtranslation_de
python train_roberta.py --corpus rj --task backtranslation_ja
python train_roberta.py --corpus rj --task backtranslation_de_ja