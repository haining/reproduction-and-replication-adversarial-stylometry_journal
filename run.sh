# ebg
python train.py --corpus ebg --task cross_validation --model svm
python train.py --corpus ebg --task cross_validation --model logistic_regression

python train.py --corpus ebg --task obfuscation --model svm
python train.py --corpus ebg --task obfuscation --model logistic_regression

python train.py --corpus ebg --task imitation --model svm
python train.py --corpus ebg --task imitation --model logistic_regression


# rj
python train.py --corpus rj --task cross_validation --model svm
python train.py --corpus rj --task cross_validation --model logistic_regression

python train.py --corpus rj --task obfuscation --model svm
python train.py --corpus rj --task obfuscation --model logistic_regression

python train.py --corpus rj --task imitation --model svm
python train.py --corpus rj --task imitation --model logistic_regression
python train.py --corpus rj --task control --model svm
python train.py --corpus rj --task control --model logistic_regression

python train.py --corpus rj --task backtranslation_de --model svm
python train.py --corpus rj --task backtranslation_de --model logistic_regression

python train.py --corpus rj --task backtranslation_ja --model svm
python train.py --corpus rj --task backtranslation_ja --model logistic_regression
python train.py --corpus rj --task backtranslation_de_ja --model svm
python train.py --corpus rj --task backtranslation_de_ja --model logistic_regression
