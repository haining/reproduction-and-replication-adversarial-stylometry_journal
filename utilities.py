"""
This module contains useful functions used to reproduce results in *Reproduction and Replication of an Adversarial
Stylometry Experiment*.
"""

import os
import re
import chardet
import numpy as np
from typing import Tuple, List
from writeprints_static import WriteprintsStatic


# possible tasks
TASKS = [
    "imitation",
    "obfuscation",
    "cross_validation",
    "control",
    "backtranslation_de",
    "backtranslation_ja",
    "backtranslation_de_ja",
    "special_english",
]

def get_data_from_rj(
        task: str,
        corpus_dir: str="resource/defending-against-authorship-attribution-corpus/corpus",
        dev: bool =False
):
    """
    Read in texts and labels from the Riddell-Juola (RJ) corpus. The RJ version contains a control group, attacks of
    imitation and obfuscation, and three round-trip translation attacks (['translation_de', 'translation_ja',
    'translation_de_ja']). The translation attacks have testing samples are translated with Google Translate and
    share the same training examples with the control group.
    Args:
        task: str, in ['control', 'imitation', 'obfuscation', 'backtranslation_de', 'backtranslation_ja',
            'backtranslation_de_ja', 'cross_validation']; when specified as 'cross_validation', all the training samples
             of ['control', 'imitation', 'obfuscation'] and no test samples will be returned
        corpus_dir: str, path to RJ corpus
        dev: bool, whether the first samples of each author is used as a dev sample, used in deep learning scenarios
    Returns:
        if not dev, a tuple of four lists, text/label of train/test sets
        else, a tuple of six lists, text/label of train/dev/test sets
    """

    def get_task_specific_data(task, dev):
        train_text_, train_label_, dev_text_, dev_label_ = [], [], [], []
        authors = [f.name.split(".")[0] for f in os.scandir(os.path.join(corpus_dir, "attacks_" + task))
                   if (not f.name.startswith(".") and f.name.split(".")[0] != 'cfeec8')]

        for dir_ in [os.path.join(corpus_dir, author) for author in authors]:
            for raw in os.scandir(dir_):
                if dev:
                    if not raw.name.endswith('_01.txt'):
                        train_text_.append(open(raw.path, 'r', encoding='utf8').read())
                        train_label_.append(raw.name.split('_')[0])
                    else:
                        dev_text_.append(open(raw.path, 'r', encoding='utf8').read())
                        dev_label_.append(raw.name.split('_')[0])
                else:
                    train_text_.append(open(raw.path, "r", encoding="utf8").read())
                    train_label_.append(raw.name.split("_")[0])
        # read in testing
        test_text_, test_label_ = zip(
            *[(open(f.path, "r", encoding=chardet.detect(open(f.path, "rb").read())["encoding"]).read(),
               f.name.split(".")[0])
                for f in os.scandir(os.path.join(corpus_dir, "attacks_" + task))
                if (".txt" in f.name and f.name.split(".")[0] != 'cfeec8')])  # cfeec8 does not have training data
        if not dev:
            return train_text_, train_label_, list(test_text_), list(test_label_)
        else:
            return train_text_, train_label_, dev_text_, dev_label_, list(test_text_), list(test_label_)

    train_text, train_label, dev_text, dev_label, test_text, test_label = [], [], [], [], [], []
    if task != "cross_validation":
        return get_task_specific_data(task, dev)
    else:
        for _task in ["imitation", "obfuscation", "control"]:
            if dev:
                train_text_, train_label_, dev_text_, dev_label_, _, _ = get_task_specific_data(_task, dev)
                train_text.extend(train_text_)
                train_label.extend(train_label_)
                dev_text.extend(dev_text_)
                dev_label.extend(dev_label_)
            else:
                train_text_, train_label_, _, _ = get_task_specific_data(_task, dev)
                train_text.extend(train_text_)
                train_label.extend(train_label_)
        if dev:
            return train_text, train_label, dev_text, dev_label, test_text, test_label
        else:
            return train_text, train_label, test_text, test_label


def get_data_from_ebg(task: str,
                      corpus_dir: str ="resource/Drexel-AMT-Corpus",
                      dev: bool =False) -> Tuple:
    """
    Read in texts and labels from the Extended Brennan-Greenstadt (EBG) corpus.
    Args:
        task: str, should be in ['obfuscation', 'imitation', 'cross_validation']
        corpus_dir: str, path to EBG corpus
        dev: bool, whether the first samples of each author is used as a dev sample, used in deep learning scenarios
    Returns:
        if not dev, a tuple of four lists, text/label of train/test sets
        else, a tuple of six lists, text/label of train/dev/test sets
    """
    train_text, train_label, dev_text, dev_label, test_text, test_label = [], [], [], [], [], []

    for author in os.scandir(corpus_dir):
        if not author.name.startswith("."):
            if dev:
                train_text_, dev_text_ = [], []
                for f in os.scandir(author.path):
                    if re.match(r'[a-z]+_[0-9]{2}_*', f.name):
                        if f.name.endswith('_01_1.txt'):
                            dev_text_.append(open(f.path, 'r', encoding=chardet.detect(open(f.path, 'rb').read())[
                                'encoding']).read())
                        else:
                            train_text_.append(open(f.path, 'r', encoding=chardet.detect(open(f.path, 'rb').read())[
                                'encoding']).read())
                train_text.extend(train_text_)
                dev_text.extend(dev_text_)
                train_label.extend([author.name] * len(train_text_))
                dev_label.extend([author.name] * len(dev_text_))
            else:
                train_text.extend(
                    [open(f.path, "r", encoding=chardet.detect(open(f.path, "rb").read())["encoding"],).read()
                     for f in os.scandir(author.path) if re.match(r"[a-z]+_[0-9]{2}_*", f.name)])
                train_label.extend([author.name] * len([f.name for f in os.scandir(author.path)
                                                        if re.match(r"[a-z]+_[0-9]{2}_*", f.name)]))
            # read in testing
            if task == "imitation":
                test_text.extend(
                    [open(f.path, "r", encoding=chardet.detect(open(f.path, "rb").read())["encoding"]).read()
                     for f in os.scandir(author.path) if re.match(r"[a-z]+_imitation_01.txt", f.name)])
                test_label.append(author.name)
            if task == "obfuscation":
                test_text.extend(
                    [open(f.path, "r", encoding=chardet.detect(open(f.path, "rb").read())["encoding"]).read()
                     for f in os.scandir(author.path) if re.match(r"[a-z]+_obfuscation.txt", f.name)])
                test_label.append(author.name)

    if dev:
        return train_text, train_label, dev_text, dev_label, test_text, test_label
    else:
        return train_text, train_label, test_text, test_label


def vectorize_writeprints_static(raws: List[str]) -> np.ndarray:
    """
    Extract `writeprints-static` features from a list of texts. Done by PyPI library `writeprints-static` v0.0.2.
    Args:
        raws: list of str.
    Returns:
         np.ndarray of writeprints-static numeric.
    """
    vec = WriteprintsStatic()
    features = vec.transform(raws)

    return features.toarray()


def vectorize_koppel512(raws: List[str]) -> np.ndarray:
    """
    Extracts `Koppel512` function words count.
    Args:
        raws: list of str.
    Returns:
         np.ndarray of Koppel512 numeric.
    """
    function_words = open("resource/koppel_function_words.txt", "r").read().splitlines()

    return np.array([[len(re.findall(r"\b" + function_word + r"\b", raw.lower()))
                    for function_word in function_words]for raw in raws])
